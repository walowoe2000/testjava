package example.tickets.controller;

import example.tickets.model.TicketOrder;
import example.tickets.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
class OrderControllerTest {

    @InjectMocks
    OrderController orderController;

    @Mock
    OrderRepository orderRepository;

    @Test
    void testAddOrder() {
        TicketOrder ticketOrder = new TicketOrder(100,new Date());
        ticketOrder.setId( (long) 1);
        when(orderRepository.save(any(TicketOrder.class))).thenReturn(ticketOrder);
        String result = orderController.addOrder(ticketOrder);
        assertThat(result.equals("1"));
        verify(orderRepository,times(1)).save(ticketOrder);
    }

    @Test
    void testGetStatus() {
        TicketOrder ticketOrder = new TicketOrder(100,new Date());
        ticketOrder.setStatus("Проведено");
        when(orderRepository.getOne((long) 1)).thenReturn(ticketOrder);
        String result = orderController.getStatus( (long) 1);
        assertThat(result.equals("Проведено"));
        verify(orderRepository,times(1)).getOne(any());
    }
}