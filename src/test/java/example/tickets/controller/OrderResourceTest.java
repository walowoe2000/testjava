package example.tickets.controller;

import example.tickets.model.TicketOrder;
import example.tickets.service.OrderService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Date;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ExtendWith(MockitoExtension.class)
class OrderResourceTest {

    @InjectMocks
    OrderResource orderResource;

    @Mock
    OrderService orderService;

    @Test
    void testCreateOrder() {
        when(orderService.addOrder(any(TicketOrder.class))).thenReturn("1");
        TicketOrder ticketOrder = new TicketOrder(100,new Date());
        String result = orderResource.create(ticketOrder);
        assertThat(result.equals("1"));
        verify(orderService,times(1)).addOrder(ticketOrder);
    }

    @Test
    void testGetStatus() {
        when(orderService.getStatus(any())).thenReturn("Проведено");
        String result = orderResource.getStatus( (long) 1 );
        assertThat(result.equals("Проведено"));
        verify(orderService,times(1)).getStatus(any());
    }
}