package example.tickets.controller;


import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer;
import example.tickets.model.TicketOrder;
import example.tickets.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("order")
@Slf4j
public class OrderResource {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public List<TicketOrder> getAll() {
        return orderService.getOrder();
    }

    @PostMapping
    public String create(@Valid @RequestBody TicketOrder ticketOrder) {
        log.info("Request create {}",ticketOrder.toString());
        return orderService.addOrder(ticketOrder);
    }

    @GetMapping("{id}")
    public String getStatus(@PathVariable Long id) {
        log.info("Request getStatus {}",id);
        return orderService.getStatus(id);
    }
}
