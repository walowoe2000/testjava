package example.tickets.controller;


import example.tickets.model.TicketOrder;
import example.tickets.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    public List<TicketOrder> getAll(){
        log.info("Request getAll");
        return paymentService.getUnprocessedOrders();
    }

    public String pay(TicketOrder ticketOrder) {
        log.info("Request pay {}",ticketOrder.toString());
        return paymentService.updateOrderStatus(ticketOrder);
    }
}
