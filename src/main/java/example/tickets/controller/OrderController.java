package example.tickets.controller;

import example.tickets.model.TicketOrder;
import example.tickets.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/resource")
@Slf4j
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @GetMapping
    public List<TicketOrder> getAll(){
        log.info("request get all");
        return orderRepository.findAll();
    }

    @PostMapping
    public String addOrder(@Validated({TicketOrder.New.class}) @RequestBody TicketOrder order){
        log.info("Request addOrder {}",order.toString());
        return String.valueOf(orderRepository.save(order).getId());
    }


    @GetMapping("{id}")
    public String getStatus(@PathVariable Long id) {
        log.info("Request getStatus {}",id);
        return orderRepository.getOne(id).getStatus();
    }
}
