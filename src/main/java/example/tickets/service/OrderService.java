package example.tickets.service;

import example.tickets.model.TicketOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Service
public class OrderService {
    private RestTemplate restTemplate;

    @Autowired
    public OrderService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public List<TicketOrder> getOrder() {
        return restTemplate.exchange("http://localhost:8080/resource", HttpMethod.GET, null,List.class).getBody();
    }

    public String addOrder(TicketOrder order) {
        HttpEntity<TicketOrder> httpEntity = new HttpEntity<>(order);
        return restTemplate.exchange("http://localhost:8080/resource", HttpMethod.POST, httpEntity,String.class).getBody();
    }

    public String getStatus(Long id) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/resource/id")
                .queryParam("id", id);
        return restTemplate.exchange("http://localhost:8080/resource/{id}", HttpMethod.GET, null,String.class,id).getBody();
    }
}
