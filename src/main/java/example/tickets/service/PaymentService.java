package example.tickets.service;

import example.tickets.model.TicketOrder;
import example.tickets.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class PaymentService {
    enum Status{
        Обрабатывается,
        Проведен,
        Ошибка,
    }
    @Autowired
    OrderRepository orderRepository;

    public List<TicketOrder> getUnprocessedOrders(){
        return orderRepository.getUnprocessedOrders();
    }

    public String updateOrderStatus(TicketOrder ticketOrder){
        ticketOrder.setStatus(Status.values()[(new Random()).nextInt(Status.values().length)].toString());
        return orderRepository.save(ticketOrder).getStatus();
    }

}
