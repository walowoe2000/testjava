package example.tickets.repository;

import example.tickets.model.TicketOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<TicketOrder, Long> {

    @Query("SELECT t FROM TicketOrder t WHERE t.status = NULL OR t.status = 'Обрабатывается'")
    List<TicketOrder> getUnprocessedOrders();
}
