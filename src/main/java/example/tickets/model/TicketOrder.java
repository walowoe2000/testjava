package example.tickets.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import example.tickets.controller.OrderResource;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.bind.DefaultValue;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@NoArgsConstructor
@Entity
@Data
public class TicketOrder {

    public interface New {
    }

    public interface Exist {
    }

    public interface UpdateStatus extends Exist {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Null(groups = {New.class})
    @NotNull(groups = {UpdateStatus.class})
    private Long id;

    @Null(groups = {UpdateStatus.class})
    @NotNull(groups = {New.class})
    @Min(1)
    private Integer routeNumber;

    @Null(groups = {New.class})
    @NotNull(groups = {UpdateStatus.class})
    private String status;

    @Null(groups = {UpdateStatus.class})
    @NotNull(groups = {New.class})
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm", timezone="GMT+02:00")
    private Date datetimeDeparture;

    public TicketOrder(Integer number, Date date) {
        this.routeNumber = number;
        this.datetimeDeparture = date;
    }
}
