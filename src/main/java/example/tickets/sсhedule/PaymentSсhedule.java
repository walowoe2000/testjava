package example.tickets.sсhedule;


import example.tickets.controller.PaymentController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PaymentSсhedule {

    @Autowired
    PaymentController paymentController;

    @Scheduled(fixedDelay = 60 * 1000, initialDelay = 30 * 1000)
    public void payAll(){
        paymentController.getAll().forEach( x -> System.out.println(paymentController.pay(x)));
    }
}
